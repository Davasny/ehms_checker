import log
import json
import config
from requests import session, post
from difflib import ndiff
from lxml.html import fromstring, HTMLParser


logger = log.set_logger("main")


class EhmsConnector:
    def __init__(self):
        self._s = session()

    def _get_tokens(self):
        r = self._s.get(config.EHMS_URL)
        if r.status_code < 400:
            tree = fromstring(r.text, parser=HTMLParser(encoding='utf-8'))
            inputs = ['login_', 'pass_']
            tokens = {}
            for name in inputs:
                tokens[name] = tree.xpath(f"//input[contains(@name, '{name}')]")[0].name

            tokens['counter'] = tree.xpath(f"//input[contains(@name, 'counter')]")[0].value
            logger.debug(tokens)
            return True, tokens
        else:
            logger.error(f'Server returned error: {r.status_code} {r.text}')
            return False, r.status_code, r.text

    def _save_cookies(self, cookies):
        logger.debug('Saving cookies')
        with open(config.AUTH_COOKIES_FILE, 'w', encoding='utf8') as f:
            f.write(json.dumps(cookies, ensure_ascii=False, indent=' '))

    def load_cookies(self, cookies):
        logger.info('Setting cookies from _auth_cookies.json file')
        self._s.cookies.update(dict(**cookies))

    def check_login_status(self):
        r = self._s.get(config.EHMS_URL)
        if 'moje dane' in r.text:
            return True
        return False

    def get_page(self, url):
        r = self._s.get(url)
        if r.status_code < 400:
            return True, r.text
        return False, r.text

    def login(self, username, password):
        tokens = self._get_tokens()
        if tokens[0]:
            r = self._s.post(
                config.EHMS_URL, data={
                    'log_form': 'yes',
                    'counter': tokens[1]['counter'],
                    tokens[1]['login_']: username,
                    tokens[1]['pass_']: password,
                }
            )
            if 'niepoprawny UID' in r.text:
                logger.critical(f'Login failed: {r.status_code} {r.text}')
                return False,
            else:
                logger.info('Logged in!')
                self._save_cookies(self._s.cookies.get_dict())
                return True,
        else:
            return tokens


if __name__ == '__main__':
    ehms = EhmsConnector()

    if len(config.AUTH_COOKIES) > 0:
        ehms.load_cookies(config.AUTH_COOKIES)
        if ehms.check_login_status():
            logger.info('Got access using cookies')

    if not ehms.check_login_status():
        login = ehms.login(config.USERNAME, config.PASSWORD)
        if not login[0]:
            raise Exception(f'Error {login}')
        logger.info('Logged in using username and password')

    # load file with old contents
    with open(config.DIFF_STORE_FILE, 'r', encoding='utf8') as f:
        try:
            diff_store = json.loads(f.read())
        except:
            diff_store = {}

    for page in config.PAGES_TO_CHECK:
        r = ehms.get_page(f'{config.EHMS_URL}{page}')
        logger.info(f'{config.EHMS_URL}{page}')
        old_string = ''
        send_notif = False

        if r[0]:
            if page in diff_store:
                old_string = diff_store[page]

                # compare old and new text
                diff = ndiff(old_string.split('\n'), r[1].split('\n'))
                changes = [l for l in diff if l.startswith('+ ') or l.startswith('- ')]
                for line in changes:
                    if 'Dziś jest' not in line:
                        logger.info('Found difference')
                        send_notif = True
                        diff_store[page] = r[1]
            else:
                diff_store[page] = r[1]

        if send_notif and config.WEBHOOK_URL is not None:
            r = post(config.WEBHOOK_URL,
                     data=f'Zmiana: {config.EHMS_URL}{page}'
                     )
            logger.debug(f'Response from webhook: {r.status_code} {r.text}')

    with open(config.DIFF_STORE_FILE, 'w', encoding='utf8') as f:
        logger.debug('Writing diff store to file')
        f.write(json.dumps(diff_store, ensure_ascii=False))
