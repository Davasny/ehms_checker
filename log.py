import logging
import config


def set_logger(name):
    level = config.LOG_LEVEL

    if level == "DEBUG":
        log_level = logging.DEBUG
    elif level == "WARNING":
        log_level = logging.WARNING
    elif level == "ERROR":
        log_level = logging.ERROR
    elif level == "CRITICAL":
        log_level = logging.CRITICAL

    logger = logging.getLogger(name)
    logger.setLevel(level)

    # setup steam handler
    sh = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(module)s - %(message)s')
    sh.setFormatter(formatter)
    logger.addHandler(sh)

    return logger
