import json
import os


EHMS_URL = 'https://ehms.pk.edu.pl/standard/'
if 'EHMS_URL' in os.environ:
    EHMS_URL = os.environ['EHMS_URL']

WEBHOOK_URL = None
if 'WEBHOOK_URL' in os.environ:
    WEBHOOK_URL = os.environ['WEBHOOK_URL']

LOG_LEVEL = "DEBUG"

DIFF_STORE_FILE = '/app/data/_diff_store.json'
AUTH_COOKIES_FILE = '/app/data/_auth_cookies.json'

USERNAME = os.environ['USERNAME']
PASSWORD = os.environ['PASSWORD']

PAGES_TO_CHECK = [
    '?tab=5&sub=8'
]

if not os.path.exists(AUTH_COOKIES_FILE):
    with open(AUTH_COOKIES_FILE, 'w+', encoding='utf8') as f:
        f.write('')

with open(AUTH_COOKIES_FILE, 'r', encoding='utf8') as f:
    try:
        AUTH_COOKIES = json.loads(f.read())
    except:
        AUTH_COOKIES = {}

# create empty diff store file
if not os.path.exists(DIFF_STORE_FILE):
    with open(DIFF_STORE_FILE, 'w+', encoding='utf8') as f:
        f.write('')
