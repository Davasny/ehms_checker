# ehms_checker

EHMS checker is a program that check every 10 minutes changes in
polish ehms system. It stores old website and checks for diff on
the new website

## Runnign
1. Set environment variables
```
export USERNAME=<username>
export PASSWORD=<password>
```
2. Fill `config.py`. If you want to use webhook pass url in file or
as environment variable
3. Install python requirements
```shell
pip install -r requirements.txt
```

## Webhook
I'm using [https://www.integromat.com/](https://www.integromat.com/s)
for integration with telegram but feel free whatever you want

